import datetime
import gridfs
import numpy as np
from pymongo import MongoClient


class Database:
    def __init__(self):
        client = MongoClient('localhost', 27017)
        db = client['satelity']
        self.testCollection = db['images']
        self.fs = gridfs.GridFS(db)

    def updateDatabase(self, image, imageIDandDate, isSnow):
        imageString = image.tostring()

        imageHash = self.fs.put(imageString, encoding='utf-8')

        dateTime = datetime.datetime.strptime(imageIDandDate[1], '%Y-%m-%dT%H:%M:%S.%fZ').replace(second=0,
                                                                                                  microsecond=0)

        key = {
            'imageID': imageIDandDate[0],
            'isSnow': isSnow,
        }

        data = {
            'imageID': imageIDandDate[0],
            'imageHash': imageHash,
            'shape': image.shape,
            'dtype': str(image.dtype),
            'date': dateTime,
            'isSnow': isSnow,
        }

        self.testCollection.update(key, data, upsert=True)

    def getImage(self, imageID, isSnow):
        image = self.testCollection.find_one(
            {
                'imageID': imageID,
                'isSnow': isSnow,
            }
        )

        gOut = self.fs.get(image['imageHash'])

        img = np.frombuffer(gOut.read(), dtype=np.uint8)

        img = np.reshape(img, image['shape'])

        return img

    def getImagesFromToDate(self, dateFrom, dateTo):
        snowImgs = self.getImagesFromToDateOnlySnow(dateFrom, dateTo)
        normalImgs = self.getImagesFromToDateOnlyNormal(dateFrom, dateTo)

        return snowImgs, normalImgs

    def getImagesFromToDateOnlySnow(self, dateFrom, dateTo):
        images = self.testCollection.find(
            {
                'date': {
                    "$gt": datetime.datetime.strptime(dateFrom, '%Y-%m-%d %H:%M'),
                    "$lt": datetime.datetime.strptime(dateTo, '%Y-%m-%d %H:%M')
                },
                'isSnow': True
            }
        )

        imgs = self.__getImages(images)

        return imgs

    def getImagesFromToDateOnlyNormal(self, dateFrom, dateTo):
        images = self.testCollection.find(
            {
                'date': {
                    "$gt": datetime.datetime.strptime(dateFrom, '%Y-%m-%d %H:%M'),
                    "$lt": datetime.datetime.strptime(dateTo, '%Y-%m-%d %H:%M')
                },
                'isSnow': False
            }
        )

        imgs = self.__getImages(images)

        return imgs

    def __getImages(self, images):
        imgs = []

        for image in images:
            gOut = self.fs.get(image['imageHash'])

            img = np.frombuffer(gOut.read(), dtype=np.uint8)

            img = np.reshape(img, image['shape'])

            imgs.append((image['imageID'], img, image['isSnow'], image['date']))

        return imgs

    def getImagesIds(self):
        return self.testCollection.find().distinct('imageID')
