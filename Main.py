from GetPhotos import PhotosGetter
import numpy as np
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QStyle
from PyQt5.QtGui import QImage, QPixmap, qRgb
from PyQt5.QtCore import Qt, QCoreApplication, QDate
from Gui import Ui_MainWindow
from rasterio.windows import Window
from Database import Database
import cv2 as cv
import sys

QCoreApplication.addLibraryPath("./")


class MainApp(QtWidgets.QMainWindow):

    def __init__(self):
        super(MainApp, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        now = QDate.currentDate()
        self.ui.edit_start_date.setText(now.toString(Qt.ISODate))
        self.ui.edit_end_date.setText(now.toString(Qt.ISODate))

        self.ui.btn_forward.setIcon(self.style().standardIcon(QStyle.SP_MediaSeekForward))
        self.ui.btn_back.setIcon(self.style().standardIcon(QStyle.SP_MediaSeekBackward))
        self.ui.radio_rgb.setChecked(True)
        self.page = 0
        self.last_page = 0

        self.image = None
        self.searched_images = None

        self.ui.btn_back.clicked.connect(self.back)
        self.ui.btn_forward.clicked.connect(self.forward)
        self.ui.btn_collect.clicked.connect(self.collect)
        self.ui.btn_search.clicked.connect(self.search)

        self.ui.radio_rgb.toggled.connect(self.radio_rgb)
        self.ui.radio_ndsi.toggled.connect(self.radio_ndsi)
        self.ui.radio_rgb_ndsi.toggled.connect(self.radio_rgb_ndsi)
        self.radio_checked = "RGB"

        self.alpha = 1
        self.beta = 0
        self.ui.slider_brightness.setRange(-100, 100)
        self.ui.slider_brightness.setValue(0)
        self.ui.slider_contrast.setRange(1, 100)
        self.ui.slider_contrast.setValue(10)
        self.ui.slider_contrast.valueChanged.connect(self.slider_contrast)
        self.ui.slider_brightness.valueChanged.connect(self.slider_brightness)

    def slider_contrast(self):
        self.alpha = self.ui.slider_contrast.value() / 10
        self.update_view()

    def slider_brightness(self):
        self.beta = self.ui.slider_brightness.value()
        self.update_view()

    def search(self):
        db = Database()
        date_from = self.ui.edit_start_date.text() + " 0:0"
        date_to = self.ui.edit_end_date.text() + " 0:0"
        self.searched_images = db.getImagesFromToDate(date_from, date_to)
        self.last_page = len(self.searched_images[0])-1
        self.ui.label_page_n.setText("/ "+str(self.last_page+1))
        self.ui.label_page.setText("1")
        self.ui.label_date.setText(str(self.searched_images[0][0][3]))
        self.update_view()

    def collect(self):
        date_from = self.ui.edit_start_date.text()
        date_to = self.ui.edit_end_date.text()
        self.collect_photos(date_from, date_to)

    def update_view(self):
        if self.searched_images is not None:
            if self.radio_checked == "RGB":
                img = self.searched_images[1][self.page][1]
            elif self.radio_checked == "NDSI":
                img = self.searched_images[0][self.page][1]
            elif self.radio_checked == "RGBNDSI":
                img = self.searched_images[1][self.page][1]

            out = cv.addWeighted(img, self.alpha, img, 0, self.beta)

            if self.radio_checked == "RGBNDSI":
                img_snow = self.searched_images[0][self.page][1]
                img_and = cv.bitwise_xor(out, 130, mask=img_snow)
                img_or = cv.bitwise_or(img_and, 50, mask=img_snow)
                out = cv.bitwise_xor(out, img_or)

            if len(out.shape) == 2:
                qim = QImage(out.data, out.shape[1], out.shape[0], out.strides[0], QImage.Format_Indexed8)
                gray_color_table = [qRgb(i, i, i) for i in range(256)]
                qim.setColorTable(gray_color_table)
            else:
                qim = QImage(out.data, out.shape[1], out.shape[0], out.strides[0], QImage.Format_RGB888)
            qpixmap = QPixmap(qim).scaled(self.ui.label_photo.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.ui.label_photo.setAlignment(Qt.AlignCenter)
            self.ui.label_photo.setPixmap(qpixmap)

    def set_image(self):
        pass

    def radio_rgb(self):
        if self.ui.radio_rgb.isChecked():
            self.radio_checked = "RGB"
            self.update_view()

    def radio_ndsi(self):
        if self.ui.radio_ndsi.isChecked():
            self.radio_checked = "NDSI"
            self.update_view()

    def radio_rgb_ndsi(self):
        if self.ui.radio_rgb_ndsi.isChecked():
            self.radio_checked = "RGBNDSI"
            self.update_view()

    def forward(self):
        if self.page < self.last_page:
            self.page += 1
            self.ui.label_page.setText(str(self.page+1))
            self.ui.label_date.setText(str(self.searched_images[0][self.page][3]))
            self.update_view()

    def back(self):
        if self.page > 0:
            self.page -= 1
            self.ui.label_page.setText(str(self.page+1))
            self.ui.label_date.setText(str(self.searched_images[0][self.page][3]))
            self.update_view()

    def collect_photos(self, date_from, date_to):
        print("Rozpoczęto pobieranie zdjęć")
        pg = PhotosGetter()
        pg.set_api_key(self.ui.edit_api_key.text())
        bbox = self.ui.edit_bbox.text()
        if bbox != '':
            pg.set_bbox(bbox)
        window1 = Window(2500, 0, 5000, 5000)
        db = Database()
        ids = pg.get_ids_by_date(date_from, date_to)
        db_ids = db.getImagesIds()
        n_ph = 1
        for i in ids:
            ph_id = i[0]
            if ph_id in db_ids:
                print("Zdjęcie znajduje sie w bazie, pomijam...")
                n_ph += 1
                continue
            print("Pobieranie zdjęcia ", n_ph, " z ", len(ids))
            print("Pobranie, przetwarzanie i zapis RGB")
            b3 = pg.get_photo_in_memory("B03", window1, ph_id)
            real_color = self.get_real_color(pg, b3, ph_id, window1)
            db.updateDatabase(real_color, i, False)
            print("Pobranie, przetwarzanie i zapis NDSI")
            snow_mask = self.get_snow_mask(pg, b3, ph_id, window1)
            db.updateDatabase(snow_mask, i, True)
            n_ph += 1
        print("Pobieranie zakończone")

    def get_real_color(self, pg, b3, i, window):
        b2 = pg.get_photo_in_memory("B02", window, i)
        b4 = pg.get_photo_in_memory("B04", window, i)
        rgb = np.dstack([
            b4,
            b3,
            b2
        ])  # złożenie 3 kanałów (macierzy) w RGB

        rgb = ((rgb * 255.0) / rgb.max()).astype(np.uint8)  # Normalizacja 16 bitowych wartosci do 8
        return rgb

    def get_snow_mask(self, pg, b3, i, window):
        window2 = Window(2500 / 2, 0, 5000 / 2, 5000 / 2)
        b8 = pg.get_photo_in_memory("B08", window, i)
        b11 = pg.get_photo_in_memory("B11", window2, i)
        b11 = cv.resize(b11, b3.shape)
        ndsi = (b3.astype(float) - b11.astype(float)) / (b3 + b11)
        ndwi = (b3.astype(float) - b8.astype(float)) / (b3 + b8)
        snow_index = ndsi - ndwi

        snow_index[snow_index < 0.72] = -1
        snow_index[snow_index > 0.72] = 1

        snow_index = (snow_index + 1) * 255 / 2.
        snow_index = np.uint8(snow_index)

        return snow_index


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    application = MainApp()
    application.show()
    sys.exit(app.exec())
