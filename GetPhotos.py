from urllib import request, error
import time
import os
import json
from rasterio.io import MemoryFile


def get_json(url):
    response = request.urlopen(url)
    data = response.read()
    encoding = response.info().get_content_charset('utf-8')
    return json.loads(data.decode(encoding))


class PhotosGetter:
    def __init__(self):
        self.api_key = ''
        self.bbox = '19.834086609060968,49.123994570376084,20.290019226248464,49.34302002289675'
        self.satellites = "Sentinel-2A,Sentinel-2B"

    def set_api_key(self, key):
        self.api_key = key

    def set_bbox(self, bbox):
        self.bbox = bbox

    def get_photo(self, band, photo_id=19008568):
        url = "https://api.spectator.earth/imagery/{p_id}/files/{band}.jp2?api_key={api_key}"\
            .format(band=band, api_key=self.api_key, p_id=photo_id)

        data_dir = "data"
        if not os.path.exists(data_dir):
            os.mkdir(data_dir)

        max_attempts = 10
        attempts = 0
        filename = "data/"+band+".jp2"
        while attempts < max_attempts:
            try:
                response = request.urlopen(url, timeout=5)
                content = response.read()
                f = open(filename, 'wb')
                f.write(content)
                f.close()
                break
            except error.URLError as e:
                attempts += 1
                print(e)
                time.sleep(1)
        return filename

    def get_photo_in_memory(self, band, window, photo_id=19008568):
        url = "https://api.spectator.earth/imagery/{p_id}/files/{band}.jp2?api_key={api_key}"\
            .format(band=band, api_key=self.api_key, p_id=photo_id)

        data_dir = "data"
        if not os.path.exists(data_dir):
            os.mkdir(data_dir)

        max_attempts = 10
        attempts = 0
        content = None
        while attempts < max_attempts:
            try:
                response = request.urlopen(url, timeout=5)
                content = response.read()
                break
            except error.URLError as e:
                attempts += 1
                print(e)

        with MemoryFile(content) as memfile:
            with memfile.open() as dataset:
                data_array = dataset.read(1, window=window)
        return data_array

    def get_last_photo(self, band):
        p_id = self.get_last_photo_id()
        self.get_photo(band, p_id)

    # Date format yyyy-mmm-dd
    def get_ids_by_date(self, start_date, end_date):
        url = 'https://api.spectator.earth/imagery/?' \
              'api_key={api_key}&bbox={bbox}&satellites={satellites}' \
              '&date_from={date_from}&date_to={date_to}&cc_less_than={cc_less_than}'\
               .format(api_key=self.api_key, bbox=self.bbox, satellites=self.satellites,
                       date_from=start_date, date_to=end_date, cc_less_than=10.0)

        JSON_object = get_json(url)
        pages = JSON_object['total_pages']
        p_ids = []
        for i in range(pages):
            results = JSON_object['results']
            for res in results:
                p_ids.append((res['id'], res['ingestion_date']))
            next_url = JSON_object['next']
            if next_url:
                JSON_object = get_json(next_url)
        return p_ids

    def get_last_photo_id(self):
        url = 'https://api.spectator.earth/imagery/?' \
              'api_key={api_key}&bbox={bbox}&satellites={satellites}'\
               .format(api_key=self.api_key, bbox=self.bbox, satellites=self.satellites)

        JSON_object = get_json(url)
        results = JSON_object['results']
        p_id = results[0]['id']
        print(p_id)
        return p_id
